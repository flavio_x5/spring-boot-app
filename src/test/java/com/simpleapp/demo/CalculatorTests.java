package com.simpleapp.demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

class CalculatorTests {

   @Test
  void testSum(){
    Calculator calculator = new Calculator();
    int result = calculator.sum(1,2);
    assertEquals(3, result);
  }

}
